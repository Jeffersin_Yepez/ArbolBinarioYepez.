package arbolbinario;

class NodoArbol {
// nombre de la clase nodo
    NodoArbol nodoIzquierdo;
//variables de nodoIzquierdo tipo nodoarbol
    int datos;
//variable de tipo entero 
    NodoArbol nodoDerecho;
// variable nodo derecho tipo nodoarbol
    
    public NodoArbol (int datosNodo){
//metodo de ingresar un dato al nodo arbol 
        datos=datosNodo;
//el dato almacene el valor de datosnodos
        nodoIzquierdo=nodoDerecho=null;
//ambos nodos seran iguales a null
    }
    
    public synchronized void insertar (int valorInsertar){
//metodo insertar un nodo dentro del arbol
        if (valorInsertar<datos){
// pregunto si el valor que ingresa ees menor a datos
            if (nodoIzquierdo==null)
//por un si entra a otro ciclo donde preguntamos si el nodoizquiero es igual a null 
                nodoIzquierdo=new NodoArbol(valorInsertar);
//si es asi insertamos el dato dentro del nodo izquierdo
            else
//sino
                nodoIzquierdo.insertar(valorInsertar);
//se sumara al nodo del mismo      
  }
        else if (valorInsertar>= datos) {
//se cierra el ciclo 
//y sino volvemos a preguntar si el valor es mayor hira x la derecha
            if (nodoDerecho==null)
//si el nodo es igual a null 
                nodoDerecho=new NodoArbol(valorInsertar);
//se alamacenara en el nodo derecho 
            else
//sino
                nodoDerecho.insertar(valorInsertar);
//se sumara al numero siguiente dentro del nodo
           }
    }

    public void busqueda(int valorBuscar)
//metodo buscar 
    {
        if(valorBuscar<datos)
//si el el dato que busco es menor al dato por un si 
        {
            if(nodoIzquierdo==null)
//preguntamos si el noto es igual a null
            {
                System.out.println("El nodo no se encuentra en el árbol");
// si es asi es menor mostrar no se encuentra en el nodo
            }
            else
//sino 
                nodoIzquierdo.busqueda(valorBuscar);
//mostrar el valor de arbol
        }
        else
//sino
        {
            if(valorBuscar>datos)
// si buscar es mayor que el dato         
	   {
                if(nodoDerecho==null)
// y el nodo derecho es igual a null
                {
                    System.out.println("El nodo no se encuentra en el árbol");
//sino mostrar que el valor no se encuentra en el arbol
                }
                else
//sino
                    nodoDerecho.busqueda(valorBuscar);
//mostrar el valor 
            }
            else
                    System.out.println("El nodo se encuentra en el árbol");
//no se encuentra el valor en el arbol 
        }
    }

    public void obtenermenor(NodoArbol nodo){
//metodo de obtener el menor 
        if (nodo==null) return;
//si el nodo es igual a null retornar
        else {
//sino
            if (nodo.nodoIzquierdo!=null){
//si nodo izquierdo es diferente o igual a null 
                obtenermenor(nodo.nodoIzquierdo);}
//mostrar el nodo menor
                else {
//sino 
                    System.out.println("el menor es "+nodo.datos);
//mostrar mensaje sobre el nodo es 
                }
            }
    }
    public  void obtenermayor(NodoArbol nodo){
//metodo de obtener mayor
      if(nodo==null) return;
//si el nodo es igual a null retorna 
      else {
//sino
          if(nodo.nodoDerecho!=null){
//preguntar si el noodo derecho es igual a null
              obtenermayor(nodo.nodoDerecho);
//obtener el valor buscado
          }
          else{
              System.out.println("El mayor es"+nodo.datos);
//mostrar mensaje donde genere el valor mayor
          }
      }
    }
}

class Arbol {
//clase arblo
    private NodoArbol raiz;
//nodo arbol sera tipo privado

    public Arbol(){
//metodo de arbol
        raiz=null;
//raiz sera igual a null
    }

    public synchronized void buscarNodo( int valorBuscar){
//el metodo buscar sera igual a 
        if (raiz!=null) raiz.busqueda(valorBuscar);
//preguntamos si la razin es diferente o igual a null
    }

    public void menor(){
//metodo obtener menot
        raiz.obtenermenor(raiz);
//llamamos al metodo con su valor
    }
    public void mayor() {
//metodo mayor
        raiz.obtenermayor(raiz);
//llamamos al metodo con su vallor
    }
    public synchronized void insertarNodo (int valorInsertar) {
//inserta 
        if (raiz==null)
//preguntamos si el valor es igual a null
            raiz=new NodoArbol (valorInsertar);
//raiz sera igual a el nodo arbol 
        else
//sino
            raiz.insertar(valorInsertar);
//almacenamos el valor a insertar
    }

    public synchronized void recorridoPreorden() {
        hacerPreorden(raiz);
    }
//mostramos el metod del preorden
    private void hacerPreorden(NodoArbol nodo){
        if (nodo==null)
            return;
        System.out.print(nodo.datos+"  "); //Vr
        hacerPreorden(nodo.nodoIzquierdo); //T1
        hacerPreorden(nodo.nodoDerecho); //T2
    }
//se le designa como debe mostrar los resultados de dicho clase y metodo
    public synchronized void recorridoPostorden() {
        hacerPostorden(raiz);
    }
// recorrido del pos orden
     private void hacerPostorden(NodoArbol nodo){
        if (nodo==null) return;
        hacerPostorden(nodo.nodoIzquierdo); //T1
        hacerPostorden(nodo.nodoDerecho); //T2
        System.out.print(nodo.datos+"  "); //Vr
    }
//se le designa como debe mostrar los resultados

    public synchronized void recorridoInorden() {
        hacerInorden(raiz);
    }
//recorrido del inorden
    private void hacerInorden(NodoArbol nodo){
        if (nodo==null) return;
        hacerInorden(nodo.nodoIzquierdo); //T1
        System.out.print(nodo.datos+"  "); //Vr
        hacerInorden(nodo.nodoDerecho); //T2
    }
}

//se le designa como debe mostrar los resultados de dicha clase y metodo
public class Main {

    
    public static void main(String[] args) {
        Arbol arbol= new Arbol();
//creamos un ojbeto de arbol que sera igual a el llamado del metodo
        int  valor1;
//creacion de variable tipo entero
        String dato;
//creacion de variables
        System.out.println("Insertando los valores: ");
//mostrar mensaje sobre lo que hace
        for (int i=1; i<=25; i++){
//utilizamos un contador para que se genere los nuemero que queremos
           //dato =javax.swing.JOptionPane.showInputDialog("valor del árbol "+i+" ");
           //valor1=Integer.parseInt(dato);

           valor1=(int) (Math.random()*15);
//este nos permite generar numeros aleatorios
           System.out.print(valor1+"  ");
           arbol.insertarNodo(valor1);
        }

        System.out.println("\n\n Recorrido preorden ");
        arbol.recorridoPreorden();
//nos muetra un mensaje de cada una de los metodos de recorrido que se utilizan
        //dato =javax.swing.JOptionPane.showInputDialog("Nodo a buscar ");
        //valor1=Integer.parseInt(dato);
        //arbol.buscarNodo(valor1);
        System.out.println("\n\n Recorrido inorden");
        arbol.recorridoInorden();
//nos muetra un mensaje de cada una de los metodos de recorrido que se utilizan
        System.out.println("\n\n Recorrido postorden ");
        arbol.recorridoPostorden();
        System.out.println(" ");
        arbol.menor();
        arbol.mayor();
        /*dato =javax.swing.JOptionPane.showInputDialog("Nodo a buscar ");
        valor1=Integer.parseInt(dato);
        arbol.buscarNodo(valor1);*/
        System.out.println(" ");
//nos muetra un mensaje de cada una de los metodos de recorrido que se utilizan


